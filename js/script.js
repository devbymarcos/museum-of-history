/*------------------------------PRE LOADER------------------------------*/
window.addEventListener('load',()=>{
    if(document.querySelector('body .preloader') != null){
        let preloader = document.querySelector('body .preloader');
        preloader.classList.add('preloaderHide');
        setTimeout(()=>{
            preloader.style.opacity = 0;
            preloader.style.display = 'none';
        },990)
    }
});

/*------------------------------EVENT RESIZE------------------------------*/
function resizeWindow(e){
    /*Home events square Image*/
    (function homeImgResize(){
        let eventImg = document.querySelectorAll('section.events-slide .event .photo');
        for(let i of eventImg){
            //console.log(i);
            i.style.height = eventImg[0].clientWidth + 'px';
        } 
    })();
}
resizeWindow();
window.addEventListener('resize',resizeWindow);



/*------------------------------HOME PAGE------------------------------*/

(function (){
    let home_top = document.querySelector('section.home_top');
    if(home_top != undefined){
        /*Home page paralax*/
        window.addEventListener('scroll',scrolling);
        function scrolling(e){
            /*Variable for home_top*/
            let paralax_offset = window.pageYOffset/3;
            home_top.style.backgroundPosition = `center -${paralax_offset}px`;
        }

    }
})()

/*------------------------------RESPONSIVE MENU------------------------------*/
let responsive_icon = document.querySelector('header img.responsive_menu_icon');
let header = document.querySelector('header');
let responsive_showed = false;
responsive_icon.addEventListener('click',()=>{
    if(responsive_showed == false){
        header.classList.add('show');
        header.classList.remove('hide');
        responsive_showed = true;
    }else{
        header.classList.add('hide');
        header.classList.remove('show');
        responsive_showed = false;
    }
});

/*------------------------------STICKY HEADER------------------------------*/

//Sticky header isn't on this site because UX reason